#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  9 11:44:35 2021

@author: aktl
"""
params = [4.71e-2,  # a0, basic contribution of CRH
          6.84e12,  # a1, max contribution of CRH
          1.78e9,  # a2, inhibition of CRH
          583,  # mu, half saturation constant of CRH
          2.28e4,  # a3, max contribution of ACTH
          1.77e5,  # a4, inhibition of ACTH
          3.81e-4,  # a5, contribution of CORT
          4.49e-2,  # omega1, elimination of CRH
          2.25e-2,  # omega2, elimination of ACTH
          2.01e-2,  # omega3, elimination of CORT
          0,  # delta, time shift
          300,  # alpha, half saturation point
          5,  # k, steepness of Hill at midpoint
          950,  # beta, half saturation point
          6,  # l, steepness in Hill at midpoint
          0.01,  # epsilon, basic contribution
          0.5217,  # Nc, normalization constant
          24*60]  # T, modulo 24 hours

params_mik = ['a0', 'a1', 'a2', 'mu', 'a3', 'a4', 'a5',
              'omega1', 'omega2', 'omega3',
              'delta', 'alpha', 'k', 'beta', 'l',
              'epsilon', 'Nc', 'T']

parameters = dict(zip(params_mik, params))
