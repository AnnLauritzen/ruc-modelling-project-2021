#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 24 15:05:31 2021

A module for loading stuff
"""
import numpy as np
import matplotlib.pyplot as plt
from itertools import product

from hpa_model import dxdt


def set_axes_equal(ax: plt.Axes):
    """
    Set 3D plot axes to equal scale.

    Make axes of 3D plot have equal scale so that spheres appear as
    spheres and cubes as cubes.  Required since `ax.axis('equal')`
    and `ax.set_aspect('equal')` don't work on 3D.
    Source: https://stackoverflow.com/a/63625222
    """
    limits = np.array([
        ax.get_xlim3d(),
        ax.get_ylim3d(),
        ax.get_zlim3d(),
    ])
    origin = np.mean(limits, axis=1)
    radius = 0.5 * np.max(np.abs(limits[:, 1] - limits[:, 0]))
    _set_axes_radius(ax, origin, radius)


def _set_axes_radius(ax, origin, radius):
    """Source: https://stackoverflow.com/a/63625222"""
    x, y, z = origin
    ax.set_xlim3d([x - radius, x + radius])
    ax.set_ylim3d([y - radius, y + radius])
    ax.set_zlim3d([z - radius, z + radius])


def trapping_region(pars):
    """return upper bounds of HPA-model trapping region"""
    x_1 = (pars['a0']+pars['a1'])/pars['omega1']
    x_2 = (pars['a3']/pars['omega2'])*x_1
    x_3 = (pars['a5']/pars['omega3'])*x_2**2
    return (x_1, x_2, x_3)


def x_in_gamma(gamma, t_array, t0, pars):
    """return x0 = x(t0) in gamma and f(x0)"""
    x = gamma[:, np.where(t_array == t0)[0][0]]
    tangent = np.array(dxdt(t0, x, pars))
    tangent = tangent / np.linalg.norm(tangent)
    return x, tangent


def make_plane(x0, normal, radius, reso):
    """return matrix (u, v, N) and surface/plane Pi and plane accuracy"""
    u = np.cross(normal, (0, 0, 1))
    u = u / np.linalg.norm(u)
    v = np.cross(normal, u)
    v = v / np.linalg.norm(u)
    M = np.column_stack((u, v, normal))

    r = np.linspace(1e-5, radius, reso)
    # r = np.linspace(1/reso, radius, reso)
    theta = np.linspace(0, 2*np.pi, reso)
    R, P = np.meshgrid(r, theta)

    J, K = R*np.cos(P), R*np.sin(P)
    Q = np.zeros(np.shape(J))

    NEW_J, NEW_K, NEW_Q = np.copy(J), np.copy(K), np.copy(Q)
    for i, j in product(range(reso), range(reso)):
        NEW_J[i, j] = M[0, 0]*J[i, j] + M[0, 1]*K[i, j] + M[0, 2]*Q[i, j]
        NEW_K[i, j] = M[1, 0]*J[i, j] + M[1, 1]*K[i, j] + M[1, 2]*Q[i, j]
        NEW_Q[i, j] = M[2, 0]*J[i, j] + M[2, 1]*K[i, j] + M[2, 2]*Q[i, j]

    sign_fix = np.zeros(np.shape(Q))
    for i, j in product(range(reso), range(reso)):
        sign_fix[i, j] = (normal[0]*(NEW_J[i, j]) +
                          normal[1]*(NEW_K[i, j]) +
                          normal[2]*(NEW_Q[i, j]))

    J, K, Q = NEW_J + x0[0], NEW_K + x0[1], NEW_Q + x0[2]

    return M, J, K, Q, sign_fix


def make_hollow_sphere(x0, r, reso):
    """return surface Sphere"""
    THETA = np.linspace(0, 2*np.pi, reso)
    PHI = np.linspace(0, np.pi, reso)
    R, P, Q = np.meshgrid(r, THETA, PHI)

    X = x0[0]+R*np.cos(P)*np.sin(Q)
    Y = x0[1]+R*np.sin(P)*np.sin(Q)
    Z = x0[2]+R*np.cos(Q)
    return X, Y, Z
