#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 22:47:12 2021

A module with the ODE model of the HPA-axis.

ODE model by: Bangsgaard, Elisabeth & Ottesen, Johnny. (2016). Patient
specific modeling of the HPA axis related to clinical diagnosis of
depression. Mathematical Biosciences. 287. 10.1016/j.mbs.2016.10.007.
"""
import numpy as np
from scipy.integrate import solve_ivp


def dxdt(t, x, pars):
    """Calculate vector field of HPA-axis model"""
    # unpack current values of x:
    x1, x2, x3 = x

    # give function for the circadian rhythm:
    C = circadian(t, pars)
    # C = 0.005217  # min(C(t))
    # C = 0.27218036647887855  # max(C(t))

    # list of dx/dt=f functions:
    derivs = [pars['a0']+C*pars['a1']/(1+pars['a2']*x3**2)*x1/(pars['mu']+x1)
              - pars['omega1']*x1,  # CRH
              pars['a3']*x1/(1+pars['a4']*x3) - pars['omega2']*x2,  # ACTH
              pars['a5']*x2**2 - pars['omega3']*x3]  # Cortisol
    return derivs


def circadian(t, pars):
    """Calculate circadian rhythm of HPA-axis model"""
    tm = np.mod(t-pars['delta'], pars['T'])
    C = (pars['Nc']*((tm**pars['k']/(tm**pars['k']+pars['alpha']**pars['k']))
                     * ((pars['T']-tm)**pars['l']
                     / ((pars['T']-tm)**pars['l']+pars['beta']**pars['l']))
                     + pars['epsilon']))
    return C


def jacobian(t, x, pars):
    """Jacobian of the vector field of the HPA-model"""
    # unpack current values of x:
    [x1, x2, x3] = x

    # give function for the circadian rhythm:
    tm = np.mod(t-pars['delta'], pars['T'])
    C = circadian(tm, pars)

    Jf = np.array(
        [[(pars['a1']*C*pars['mu'])/(
            (1+pars['a2']*x3**2)*(pars['mu']+x1)**2)-pars['omega1'],  # Jf[1,1]
        0.,                                                           # Jf[1,2]
        (-2*x3*pars['a2']*pars['a1']*x1*C)/(
            (1+pars['a2']*x3**2)**2*(pars['mu']+x1))],                # Jf[1,3]

        [pars['a3']/(1+pars['a4']*x3),                                # Jf[2,1]
        -pars['omega2'],                                              # Jf[2,2]
        (-pars['a4']*pars['a3']*x1)/(1+pars['a4']*x3)**2],            # Jf[2,3]

        [0.,                                                          # Jf[3,1]
        2*pars['a5']*x2,                                              # Jf[3,2]
        -pars['omega3']]])                                            # Jf[3,3]
    return Jf


def dxdt_withJacobian(t, x, pars):
    """Calculate vector field simultaniously with jacobian"""
    # unpack current values of x:
    [x1, x2, x3,
     a11, a12, a13,
     a21, a22, a23,
     a31, a32, a33] = x

    # give function for the circadian rhythm:
    tm = np.mod(t-pars['delta'], pars['T'])
    C = circadian(tm, pars)

    Jf = jacobian(t, [x1, x2, x3], pars)

    Jphi = np.reshape(x[3:, ], (3, 3))
    Jf_Jphi = np.matmul(Jf, Jphi)
    Jf_Jphi = np.reshape(Jf_Jphi, (9))

    derivs = np.empty(shape=(12), dtype=float)
    derivs[:3] = [
        pars['a0']+C*pars['a1']/(1+pars['a2']*x3**2)*x1/(pars['mu']+x1)
        - pars['omega1']*x1,                                  # dx1/dt
        pars['a3']*x1/(1+pars['a4']*x3) - pars['omega2']*x2,  # dx2/dt
        pars['a5']*x2**2 - pars['omega3']*x3]                 # dx3/dt
    derivs[3:] = Jf_Jphi                                    # Jf(J(phi))

    return derivs


def solve(dxdt, t, x0, pars):
    """Solve the differential equations with a specified time-step."""
    # Call the ODE solver
    sol = solve_ivp(fun=dxdt, t_span=[t[0], t[-1]], y0=x0, method='BDF',
                    t_eval=t, args=(pars,),
                    rtol=1e-10, atol=1e-10)
    return sol.y
