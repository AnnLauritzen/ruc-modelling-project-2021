# Modelling Project, Autumn 2021

A student project by Mikkel Zielinski Ajslev & Ann Katrine Toft Lauritzen, Roskilde University, 2021.
Link to project report: https://rucforsk.ruc.dk/ws/files/78030171/ModellingProjectMasters.pdf

HOW TO RUN THE CODE:
1) Run "data.py". Change the initial value t_0 on line 81, and the resulotion on line 76 (max recommended resolution is 20)
2) Run "analysis.py". Change the time t on line 47

## Acknowledgements

Supervision: Carsten Lunde Petersen, Associate Professor, Mathematics and Physics (IMFUFA)

ODE model: Bangsgaard, Elisabeth & Ottesen, Johnny. (2016). Patient specific modeling of the HPA axis related to clinical diagnosis of depression. Mathematical Biosciences. 287. 10.1016/j.mbs.2016.10.007. 

Function "set_axes_equal" and "_set_axes_radius" written by AndrewCox: https://stackoverflow.com/a/63625222

PEP 8 -- Style Guide for Python Code (Spyder 4.2.1): https://www.python.org/dev/peps/pep-0008/
