#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 13:42:34 2021

ANALYSE AND GRAPH

@author: aktl and majslev
"""
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.gridspec import GridSpec

from module import *

print(' -------------- Initialise script -------------- ')

# load data made with data.py
tInc = np.load('data/tInc.npy')
t_gamma = np.load('data/t_gamma.npy')
C_crit = np.load('data/C_crit.npy')
C = np.load('data/C.npy')
gamma = np.load('data/gamma.npy')
t0 = np.load('data/t0.npy')
resolution = np.load('data/resolution.npy')
x0 = np.load('data/x0.npy')
tangent = np.load('data/tangent.npy')
Pi_X = np.load('data/Pi_X.npy')
Pi_Y = np.load('data/Pi_Y.npy')
Pi_Z = np.load('data/Pi_Z.npy')
Sphere_X = np.load('data/Sphere_X.npy')
Sphere_Y = np.load('data/Sphere_Y.npy')
Sphere_Z = np.load('data/Sphere_Z.npy')
t_array = np.load('data/t_array.npy')
phi_Pi = np.load('data/phi_Pi.npy')
phi_Sphere = np.load('data/phi_Sphere.npy')
colours = np.load('data/colours.npy')
x0_Jphi0 = np.load('data/x0_Jphi0.npy')
x_Jphi = np.load('data/x_Jphi.npy')
Jphi = np.load('data/Jphi.npy')
lower_bound = np.load('data/lower_bound.npy')
upper_bound = np.load('data/upper_bound.npy')
# = np.load('data/.npy')

# choose time t to investigate
run_hours = 3
t = int(60*run_hours)

# Pick an example to be plotted:
EXAMPLE = (0, resolution-1)

# %%
print(' -------------- Create surface phi(t, Pi) -------------- ')

# For all of the solutions, get the coordinate after t hours:
phi_Pi_t = phi_Pi[:, :, :, t_array == t0 + t]

# %%
print(' -------------- Create surface phi(t, Sphere) -------------- ')

# For all of the solutions, get the coordinate after t hours:
phi_Sphere_t = phi_Sphere[:, :, :, :, t_array == t0 + t]

# %%
print(' -------------- Create matrix Jphi(t0+t) -------------- ')

Jphi0 = np.reshape(x_Jphi[3:, 0], (3, 3))
Jphi_t = np.reshape(x_Jphi[3:, t_array == t0 + t], (3, 3))
print('Jphi(t0+t):')
print(Jphi_t)
print('det(Jphi(t0+t)):', np.linalg.det(Jphi_t))
print('norm of each column of Jphi(t0+t)',  np.linalg.norm(Jphi_t, axis=0))
print('two-norm of Jphi(t0+t)',  np.linalg.norm(Jphi_t))sns.color_palette("colorblind")

# find eigenvalues of Jphi(t) by two methods:
# 1) pen and paper calculated polynomial:
coefficients = [
    -1,

    Jphi_t[0, 0] + Jphi_t[1, 1] + Jphi_t[2, 2],

    (-Jphi_t[0, 0]*(Jphi_t[1, 1] + Jphi_t[2, 2])-Jphi_t[1, 1]*Jphi_t[2, 2]
     + Jphi_t[1, 2]*Jphi_t[2, 1]+Jphi_t[0, 1]*Jphi_t[1, 0]
     + Jphi_t[0, 2]*Jphi_t[2, 0]),

    (Jphi_t[0, 0]*(Jphi_t[1, 1]*Jphi_t[2, 2] - Jphi_t[1, 2]*Jphi_t[2, 1])
     + Jphi_t[0, 1]*(Jphi_t[1, 2]*Jphi_t[2, 0] - Jphi_t[1, 0]*Jphi_t[2, 2])
     + Jphi_t[0, 2]*(Jphi_t[1, 0]*Jphi_t[2, 1] - Jphi_t[2, 0]*Jphi_t[1, 1]))
    ]
roots = np.roots(coefficients)

# and 2) Numpy code:
eig = np.linalg.eig(Jphi_t)

for i in range(len(eig[0])):
    print('check if the two eigenvalues are the same:')
    print('eig val 1:', roots[i])
    print('eig val 2:', eig[0][i])
    print('eig vec:', np.transpose(eig[1][:, i]))

# %%
print(' -------------- Plotting figures -------------- ')
plt.close('all')
plt.rcParams['axes.grid'] = True
sns.set_palette("colorblind")

# %% CIRCADIAN

fig4 = plt.figure(4, figsize=(8, 3))
plt.title('The circadian function C(t)')

plt.plot(t_gamma/60, C, label=r'$C(t)$')
plt.plot(t_gamma/60, C_crit, '--', label=r'$C_{crit}$')
# plt.plot(t_gamma[np.where(C <= 0.0728)]/60, C[np.where(C <= 0.0728)],
#          'r.', label=r'$C<C_{crit}$')
plt.xticks(np.arange(0, 25, 3))
plt.xlim(0, 24)
plt.xlabel('Time in hours')
plt.ylabel('C')
plt.legend()
plt.tight_layout()
plt.show()

# %% TINY PLOT OF GAMMA (FOR THE REPORT PAGE 2)

# fig7 = plt.figure(7, figsize=(4, 3))
# ax = fig7.gca(projection='3d')

# ax.plot(gamma[0], gamma[1], gamma[2], '-')

# ax.grid(True)
# ax.view_init(elev=22, azim=-45-90*1)
# # ax.set_box_aspect([1, 1, 1])
# # set_axes_equal(ax)

# ax.set_xticklabels([])
# ax.set_yticklabels([])
# ax.set_zticklabels([])

# ax.set_xlabel(r'$x_1$')
# ax.set_ylabel(r'$x_2$')
# ax.set_zlabel(r'$x_3$')
# plt.tight_layout()
# plt.show()

# %% GAMMA

fig3 = plt.figure(3, figsize=(8, 5))
fig3.suptitle('The periodic orbit $\gamma$ of the HPA-axis model')

gs = GridSpec(3, 2, figure=fig3)
ax1 = fig3.add_subplot(gs[0, 0])
ax2 = fig3.add_subplot(gs[1, 0])
ax3 = fig3.add_subplot(gs[2, 0])
ax4 = fig3.add_subplot(gs[:, 1], projection='3d')

ax1.plot(t_gamma/60, gamma[0])
ax1.plot(t_gamma[0]/60, gamma[0, 0],
         '*', label='initial value')
ax1.set_ylabel(r'$\phi_1$')
ax1.set_xticks(np.arange(0, 25, 3))
ax1.set_xlim(-1, 24)
ax1.legend()

ax2.plot(t_gamma/60, gamma[1])
ax2.plot(t_gamma[0]/60, gamma[1, 0],
         '*', label='initial value')
ax2.set_ylabel(r'$\phi_2$')
ax2.set_xticks(np.arange(0, 25, 3))
ax2.set_xlim(-1, 24)
ax2.legend()

ax3.plot(t_gamma/60, gamma[2])
ax3.plot(t_gamma[0]/60, gamma[2, 0],
         '*', label='initial value')
ax3.set_xlabel('time in hours')
ax3.set_ylabel(r'$\phi_3$')
ax3.set_xticks(np.arange(0, 25, 3))
ax3.set_xlim(-1, 24)
ax3.legend()

ax4.plot(gamma[0], gamma[1], gamma[2],
         label='$\gamma$')
ax4.plot(gamma[0, 0], gamma[1, 0], gamma[2, 0],
          '*', label='initial value')
# ax4.plot(gamma[0][np.where(C <= 0.0728)],
#           gamma[1][np.where(C <= 0.0728)],
#           gamma[2][np.where(C <= 0.0728)],
#           'r,', label='stable part ($C<C_{crit}$)')
ax4.view_init(elev=22, azim=-45)
# ax4.set_box_aspect([1, 1, 1])
# set_axes_equal(ax4)
ax4.set_xlabel(r'$\phi_1$')
ax4.set_ylabel(r'$\phi_2$')
ax4.set_zlabel(r'$\phi_3$')
ax4.legend()

plt.show()

# %% POINCARÉ

fig2 = plt.figure(2, figsize=(6, 5))
ax = fig2.gca(projection='3d')
ax.set_title('Colourised Poincaré section at $x_0 = x(t_0)$')

ax.plot(gamma[0], gamma[1], gamma[2],
        '-', label=r'orbit $\gamma$')

ax.plot(x0[0], x0[1], x0[2], '*', label='$x_0$', zorder=10)
ax.quiver(x0[0], x0[1], x0[2], tangent[0], tangent[1], tangent[2],
          label='Surface normal $N$', zorder=10)

# ax.plot(phi_Pi[EXAMPLE[0], EXAMPLE[1], 0, :],
#         phi_Pi[EXAMPLE[0], EXAMPLE[1], 1, :],
#         phi_Pi[EXAMPLE[0], EXAMPLE[1], 2, :],
#         'r--', label='example of solution', zorder=10)
# ax.plot(phi_Pi[EXAMPLE[0], EXAMPLE[1], 0, 0],
#         phi_Pi[EXAMPLE[0], EXAMPLE[1], 1, 0],
#         phi_Pi[EXAMPLE[0], EXAMPLE[1], 2, 0],
#         'ro', label='initial value of example of solution', zorder=10)

ax.plot_surface(Pi_X, Pi_Y, Pi_Z, facecolors=colours)

ax.view_init(elev=22, azim=-45)

# -------- USE THIS TO ZOOM IN AROUND t0
zoom = 1
centre = np.where(t_gamma == t0)[0][0]
ax.set_xlim(gamma[0, centre]-zoom, gamma[0, centre]+zoom)
ax.set_ylim(gamma[1, centre]-zoom, gamma[1, centre]+zoom)
ax.set_zlim(gamma[2, centre]-zoom, gamma[2, centre]+zoom)

ax.set_box_aspect([1, 1, 1])
set_axes_equal(ax)

ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
ax.set_zlabel(r'$x_3$')
ax.legend(loc='upper right')
plt.tight_layout()
plt.show()

# %% phi(t,Pi) and phi(t,Sphere)

fig5 = plt.figure(5, figsize=(7, 6))
ax = fig5.gca(projection='3d')
ax.set_title('Surfaces in the HPA-model phase space')

ax.plot(gamma[0], gamma[1], gamma[2],
        '-', label=r'orbit $\gamma$')

# -------- phi(t,Pi):
# ax.scatter(Pi_X, Pi_Y, Pi_Z,
#             alpha=0.3, label=r'$\Pi=\phi(t_0,\Pi)$')
# ax.scatter(phi_Pi_t[:, :, 0],
#             phi_Pi_t[:, :, 1],
#             phi_Pi_t[:, :, 2],
#             alpha=0.3, label=r'$\phi(t_0+t,\Pi)$')

# -------- phi(t,Sphere)
ax.scatter(Sphere_X, Sphere_Y, Sphere_Z,
            alpha=0.3, label=r'$\mathbb{S}^2=\phi(t_0,\mathbb{S}^2)$')
ax.scatter(phi_Sphere_t[:, :, :, 0],
            phi_Sphere_t[:, :, :, 1],
            phi_Sphere_t[:, :, :, 2],
            alpha=0.3, label=r'$\phi(t_0+t,\mathbb{S}^2)$')

# -------- USE THIS TO ZOOM IN AROUND t0+t
# zoom = 10
# centre = np.where(t_gamma == (t0 + t))[0][0]
# ax.set_xlim(gamma[0, centre]-zoom, gamma[0, centre]+zoom)
# ax.set_ylim(gamma[1, centre]-zoom, gamma[1, centre]+zoom)
# ax.set_zlim(gamma[2, centre]-zoom, gamma[2, centre]+zoom)

ax.view_init(elev=22, azim=-45)
# ax.set_box_aspect([1, 1, 1])
# set_axes_equal(ax)

ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
ax.set_zlabel(r'$x_3$')
ax.legend(loc='upper right')
plt.tight_layout()
plt.show()

# %% J(phi)

fig6 = plt.figure(6, figsize=(6, 5))
ax = fig6.gca(projection='3d')
ax.set_title('Transformation $J\phi$ visualised')

ax.plot(gamma[0], gamma[1], gamma[2],
        '-', label=r'orbit $\gamma$')

ax.plot(x0[0], x0[1], x0[2],
        '.', color=(0.8705882352941177,
                    0.5607843137254902,
                    0.0196078431372549),
        markersize=12, label='$x_0$ and $J\phi_{(t_0,x_0)}$')

for i in range(3):
    ax.quiver(x0[0], x0[1], x0[2],
              Jphi0[0, i], Jphi0[1, i], Jphi0[2, i],
              color=(0.8705882352941177,
                     0.5607843137254902,
                     0.0196078431372549),
              zorder=100)

x1 = gamma[:, int(np.where(t_gamma == np.mod(t0 + t, 60*24))[0])]
ax.plot(x1[0], x1[1], x1[2],
        '.', color=(0.00784313725490196,
                    0.6196078431372549,
                    0.45098039215686275),
        markersize=12, label='$x(t_0+t)$ and $J\phi_{(t_0+t,x_0)}$')

for i in range(3):
    ax.quiver(x1[0], x1[1], x1[2],
              Jphi_t[0, i], Jphi_t[1, i], Jphi_t[2, i],
              color=(0.00784313725490196,
                     0.6196078431372549,
                     0.45098039215686275),
              zorder=100)

# -------- USE THIS TO ZOOM IN AROUND t0+t
# zoom = 1
# centre = np.where(t_gamma == (t0 + t))[0][0]
# ax.set_xlim(gamma[0, centre]-zoom, gamma[0, centre]+zoom)
# ax.set_ylim(gamma[1, centre]-zoom, gamma[1, centre]+zoom)
# ax.set_zlim(gamma[2, centre]-zoom, gamma[2, centre]+zoom)

ax.view_init(elev=22, azim=-45)
ax.set_box_aspect([1, 1, 1])
set_axes_equal(ax)

ax.set_xlabel(r'$x_1$')
ax.set_ylabel(r'$x_2$')
ax.set_zlabel(r'$x_3$')
ax.legend()
plt.tight_layout()
plt.show()

# %% BOUNDS ON Jphi_t

fig8 = plt.figure(8, figsize=(6, 4))
plt.title('Bounds for supremum norm of transformation $J\phi_{(t,x_0)}$')

plt.plot(t_array/60, upper_bound, '-', label=r'upper bound')
plt.plot(t_array/60, lower_bound, '--', label=r'lower bound')
plt.plot(t_array/60, upper_bound-lower_bound, '-.', label=r'upper - lower')
plt.plot(t_array/60, np.ones(shape=np.size(t_array/60)), label=r'1')
plt.yscale('log')
plt.xticks(np.arange(t0/60, t0/60+24*2+1, 3))
plt.xlim(t0/60, t0/60+24*2)
plt.xlabel('t [hours]')
plt.ylabel(r'norm($J\phi_{(t,x_0)}$)')
plt.legend()
plt.tight_layout()
plt.show()
