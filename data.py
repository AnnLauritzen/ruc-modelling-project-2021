#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  2 13:36:54 2021

CREATE DATA

@author: aktl and majslev
"""
import numpy as np
from timeit import default_timer as timer
from itertools import product
import seaborn as sns

from hpa_model import dxdt, solve, circadian, dxdt_withJacobian
from hpa_parameters import parameters
from module import *

sns.set_palette("colorblind")
# View RGB values of palette with:
# sns.color_palette("colorblind")

print(' \n -------------- Initialise script -------------- ')

tInc = 0.1  # time-step precision
T = np.int(60*24/tInc)  # period of gamma
t_gamma = np.arange(0, 60*24, tInc)  # time array of length T

# Calculate the upper limits of the variables:
upper_bounds = trapping_region(pars=parameters)

np.save('data/tInc', tInc)
np.save('data/t_gamma', t_gamma)

# %%
print(' \n -------------- Investigate function C(t) -------------- ')

C = circadian(t_gamma, parameters)
C_crit = np.ones(len(t_gamma))*0.0728

print('C min:', min(C))
print('C max:', max(C))
print('C crit: 0.0728')

np.save('data/C_crit', C_crit)
np.save('data/C', C)

# %%
print(' \n -------------- Create orbit gamma -------------- ')
ping = timer()

# Start somewhere that seems reasonable for t_0:
x0 = [2.0, 8.5, 1.3]
# x0 = np.array(upper_bounds)

# Run for 10 days:
t0 = 0.0
tStop = 60.*24*10
t_array = np.arange(t0, tStop, tInc)

# Solve the ODE model:
sol = solve(dxdt, t_array, x0, parameters)
# Check if the orbit is closed:
check = np.linalg.norm(sol[:, -1-T]-sol[:, -1])
print('are we on the periodic orbit yet? Is', check, '= 0?')

# Return the last 24 hours, and treat this as gamma:
gamma = sol[:, -T:]

np.save('data/gamma', gamma)

print(" ------ {} Seconds needed for that ------".format(timer() - ping))

# %%
print(' \n -------------- Initialise solving -------------- ')

# Pick a point for the surface of initial values:
t0 = 60.*3
print('t0 =', int(t0/60), 'o\'clock')
# Pick a resolution for the surface of initial values:
resolution = 5
print('resolution of surfaces:', resolution)
# Pick how big the surface of initial values should be:
radius = 1
print('radius of surfaces:', radius)

# Pick how long solutions should run:
t = 60.*24*2

np.save('data/t0', t0)
np.save('data/resolution', resolution)

# %%
print(' \n -------------- Create surfaces Pi and Sphere -------------- ')

# get x0 = x(t_0) in gamma
x0, tangent = x_in_gamma(gamma, t_gamma, t0, parameters)

# surface Pi
M, Pi_X, Pi_Y, Pi_Z, plane_accuracy = make_plane(
    x0=x0,
    normal=tangent,
    radius=radius,
    reso=resolution)

print('plane accuracy:', np.max(plane_accuracy), np.min(plane_accuracy))
print('normalised tangent at x0:', tangent)
print('matrix (u, v, N):')
print(M)

# surface Sphere
Sphere_X, Sphere_Y, Sphere_Z = make_hollow_sphere(
    x0=x0,
    r=radius,
    reso=resolution)

np.save('data/x0', x0)
np.save('data/tangent', tangent)
np.save('data/Pi_X', Pi_X)
np.save('data/Pi_Y', Pi_Y)
np.save('data/Pi_Z', Pi_Z)
np.save('data/Sphere_X', Sphere_X)
np.save('data/Sphere_Y', Sphere_Y)
np.save('data/Sphere_Z', Sphere_Z)

# %%
print(' \n -------------- Create solutions phi to HPA-model -------------- ')
ping = timer()

# t_array = np.arange(t0, t0 + t, tInc)
# APPARENTLY WE CAN AVOID ROUNDING ERRORS WITH LINSPACE ...
t_array = np.linspace(t0, t0 + t, int(t/tInc), endpoint=False)

# solutions to surface Pi
phi_Pi = np.empty(shape=(resolution, resolution, 3, int(t/tInc)))

for i, j in product(range(resolution), range(resolution)):
    phi_Pi[i, j, :, :] = solve(dxdt, t_array, (Pi_X[i, j],
                                         Pi_Y[i, j],
                                         Pi_Z[i, j]), parameters)

# solutions to surface Sphere
phi_Sphere = np.empty(shape=(np.shape(Sphere_X)[0],
                             np.shape(Sphere_X)[1],
                             np.shape(Sphere_X)[2],
                             3, int(t/tInc)))

for i, j, k in product(range(np.shape(Sphere_X)[0]),
                       range(np.shape(Sphere_X)[1]),
                       range(np.shape(Sphere_X)[2])):
    phi_Sphere[i, j, k, :, :] = solve(dxdt, t_array, (Sphere_X[i, j, k],
                                                Sphere_Y[i, j, k],
                                                Sphere_Z[i, j, k]), parameters)

np.save('data/t_array', t_array)
np.save('data/phi_Pi', phi_Pi)
np.save('data/phi_Sphere', phi_Sphere)

print("------ {} Seconds needed for that ------".format(timer() - ping))

# %%
print(' \n -------------- Create colourised Poincaré section -------------- ')

t_interval = 1
diff_start = T - int(60*t_interval/tInc)
diff_end = T + int(60*t_interval/tInc)
print('check: does phi_Pi(diff_end) exist?', phi_Pi[i, j, :, diff_end-1])

colours = np.full(shape=Pi_X.shape, dtype=str, fill_value='c')

for i, j in product(range(np.shape(phi_Pi)[0]), range(np.shape(phi_Pi)[1])):
    inner_product = (tangent[0]*(phi_Pi[i, j, 0, diff_start:diff_end]-x0[0])
                     + tangent[1]*(phi_Pi[i, j, 1, diff_start:diff_end]-x0[1])
                     + tangent[2]*(phi_Pi[i, j, 2, diff_start:diff_end]-x0[2]))
    sign = np.diff(np.sign(inner_product))
    sign_time = np.where(sign == 2)[0]

    for k in sign_time:
        dist_start = np.linalg.norm(x0-phi_Pi[i, j, :, 0])
        dist_end = np.linalg.norm(x0-phi_Pi[i, j, :, diff_start+k+1])
        dist_end_fix = np.linalg.norm(x0-phi_Pi[i, j, :, diff_start+k])
        if (dist_end <= dist_start or dist_end_fix <= dist_start):
            colours[i, j] = 'g'
            break
        else:
            print('long distance:', dist_end-dist_start)
            colours[i, j] = 'r'

np.save('data/colours', colours)

# %%
print(' \n -------------- Investigate transformation J(phi) -------------- ')

x0_Jphi0 = [
    x0[0], x0[1], x0[2],
    1, 0, 0,
    0, 1, 0,
    0, 0, 1]

x_Jphi = solve(
    dxdt=dxdt_withJacobian,
    t=t_array,
    x0=x0_Jphi0,
    pars=parameters)

Jphi = np.reshape(x_Jphi[3:, :], (3, 3, np.shape(x_Jphi)[1]))
lower_bound = np.empty(shape=np.shape(Jphi)[2])
upper_bound = np.empty(shape=np.shape(Jphi)[2])
for i in range(np.shape(Jphi)[2]):
    lower_bound[i] = np.max(np.linalg.norm(Jphi[:, :, i], axis=0))
    upper_bound[i] = np.linalg.norm(Jphi[:, :, i])

np.save('data/x0_Jphi0', x0_Jphi0)
np.save('data/x_Jphi', x_Jphi)
np.save('data/Jphi', Jphi)
np.save('data/lower_bound', lower_bound)
np.save('data/upper_bound', upper_bound)
